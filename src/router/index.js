import Vue from 'vue'
import VueRouter from 'vue-router'
/* 先配置路由 */
// 引入一级路由
import Login from '@/views/login'
import Register from '@/views/register'
import Layout from '@/views/layout'
import Collect from '@/views/collect'
import Rent from '@/views/rent'
import City from '@/views/city'
import GotoRent from '@/views/gotorent'
import XqName from '@/views/xqName'
import Map from '@/views/map'
// 引入二级路由
import Index from '@/views/index'
import Find from '@/views/find'
import News from '@/views/news'
import My from '@/views/my'
import { getToken } from '@/utils/storage'

Vue.use(VueRouter)
// 配置路由规则
const routes = [
  {
    path: '/',
    component: Layout,
    redirect: '/index', // 重定向
    // 二级路由
    children: [
      {
        path: '/index',
        component: Index
      },
      {
        path: '/find',
        component: Find
      },
      {
        path: '/news',
        component: News
      },
      {
        path: '/my',
        component: My
      }

    ]
  },
  {
    path: '/xqname',
    component: XqName
  },
  {
    path: '/gotorent',
    component: GotoRent
  },
  {
    path: '/city',
    component: City
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/register',
    component: Register
  },
  {
    path: '/collect',
    component: Collect
  },
  {
    path: '/rent',
    component: Rent
  },
  {
    path: '/map',
    component: Map
  }
]

const router = new VueRouter({
  routes
})

// 页面访问拦截 全局前置守卫
// 白名单
const whiteList = ['/login', 'register']
router.beforeEach = (to, from, next) => {
  if (getToken()) {
    next()
  } else if (whiteList.includes(to.path)) {
    next()
  } else {
    next('/login')
  }
}
export default router
