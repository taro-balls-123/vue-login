import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { Form, Field, Cell, Button, NavBar, Tabbar, TabbarItem, Icon, Toast, Image as VanImage, Grid, GridItem, Card, Tag, List, Dialog, Search, Swipe, SwipeItem, Lazyload, IndexBar, IndexAnchor, Popup, Uploader, Picker, DropdownMenu, DropdownItem } from 'vant'
import BaiduMap, { BmlMarkerClusterer } from 'vue-baidu-map'

Vue.use(DropdownMenu)
Vue.use(DropdownItem)
Vue.component('bml-marker-cluster', BmlMarkerClusterer)
Vue.use(BaiduMap, {
  // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
  ak: 'ruWhG7zBVfhcUER4cw7pPkGz0F6DsAcb'
})
Vue.use(Picker)
Vue.use(Uploader)
Vue.use(Popup)
Vue.use(IndexBar)
Vue.use(IndexAnchor)
Vue.use(Lazyload)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Search)
Vue.use(Cell)
Vue.use(Dialog)
Vue.use(List)
Vue.use(Tag)
Vue.use(Card)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(VanImage)
Vue.use(Toast)
Vue.use(Icon)
Vue.use(Tabbar)
Vue.use(TabbarItem)

Vue.use(NavBar)

Vue.use(Button)

Vue.use(Form)
Vue.use(Field)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
