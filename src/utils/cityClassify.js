// 扒的城市分类方法
export const pySegSort = (arr) => {
  if (!String.prototype.localeCompare) return null
  // let letters = 'abcdefghjklmnopqrstwxyz'.split('')
  const letters = 'ABCDEFGHJKLMNOPQRSTWXYZ'.split('')
  const zh = '阿八嚓哒妸发旮哈讥咔垃痳拏噢妑七呥扨它穵夕丫帀'.split('')
  const segs = []
  letters.forEach((item, i) => {
    const cur = { letter: item, data: [] }
    arr.forEach((item) => {
      if (item.localeCompare(zh[i]) >= 0 && item.localeCompare(zh[i + 1]) < 0) {
        cur.data.push(item)
      }
    })
    if (cur.data.length) {
      cur.data.sort(function (a, b) {
        return a.localeCompare(b, 'zh')
      })
      segs.push(cur)
    }
  })

  return segs
}
