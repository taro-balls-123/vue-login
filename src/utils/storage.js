
// 本地存储Token

const key = 'myToken'
export const setToken = (newToken) => {
  localStorage.setItem(key, newToken)
}

export const getToken = () => {
  return localStorage.getItem(key)
}

export const delToken = () => {
  localStorage.removeItem(key)
}

// 存城市名字
const key1 = 'cityname'
export const setcityName = (cityname) => {
  localStorage.setItem(key1, cityname)
}
export const getcityName = () => {
  return localStorage.getItem(key1)
}

export const delcityName = () => {
  localStorage.removeItem(key1)
}

// 存城市地址 id
const key2 = 'cityValue'
export const setcityValue = (cityValue) => {
  localStorage.setItem(key2, cityValue)
}
export const getcityValue = () => {
  return localStorage.getItem(key2)
}
export const delcityValue = () => {
  localStorage.removeItem(key2)
}
