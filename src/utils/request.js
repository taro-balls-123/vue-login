// 封装axios 用于发送请求

import router from '@/router'
import axios from 'axios'
// import { Toast } from 'vant'
import { delToken, getToken } from './storage'
// 创建一个新的axios实例
const request = axios.create({
  baseURL: 'http://liufusong.top:8080',
  timeout: 50000
})

// 添加请求拦截器
request.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么

  // 1.给每一个请求添加token

  config.headers.authorization = getToken()
  return config
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error)
})

// 添加响应拦截器
request.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response.data
}, function (error) {
  // 统一错误提示
  if (error.response) {
    // 2.有错误响应 删掉token 调到登录页面
    if (error.response.status === 400) {
      delToken()
      router.push('/login')
    } else {
      // Toast(error.response.data.message)
    }
  }
  // 对响应错误做点什么
  return Promise.reject(error)
})

export default request
