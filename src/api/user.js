// 封装api接口  发送axios请求
import request from '@/utils/request'
// 按需导出
// 暴露出注册请求
export const register = data => {
  return request.post('/user/registered', data)
}
// 暴露出登录请求
export const login = data => {
  return request.post('/user/login', data)
}
// 用户信息请求
export const userInfo = () => {
  return request.get('/user', {
  })
}
// 我的收藏
export const collectList = () => {
  return request.get('/user/favorites')
}
// 我的出租
export const rentList = () => request.get('/user/houses')
// 城市列表
export const cityList = () => request.get('/area/city?level=1')
// 热门城市
export const cityHot = () => request.get('/area/hot')
// 当前城市获取信息
export const cityNow = (name) => request.get('/area/info?', { params: { name } })/* params里面必须是个对象 */
// 小区搜索
export const xqName = (data) => request.get('/area/community', {
  params: data
})
